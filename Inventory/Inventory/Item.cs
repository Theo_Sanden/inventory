﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory
{
    class Item
    {
        private string name;
        private string info;
        public String Name
        {
            get { return name;}
        }
        public string GetInfo()
        {
            return info;
        }
        public virtual void Use(Character target)
        {

        }
    }
}
