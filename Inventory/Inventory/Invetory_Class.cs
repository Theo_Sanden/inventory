﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory
{
    class Invetory_Class
    {
        private List<Item> items;

        public void Add(Item item)
        {
            items.Add(item);
        }
        public void Remove(int index)
        {
            if(CheckIfItemExists(index))
             items.Remove(items[index]);
          
           
            
            
        }
        public string GetItemInfo(int index)
        {
            if (CheckIfItemExists(index))
                return items[index].GetInfo();
            return "";

        }
        public void UseItem(int index, Character target)
        {
            if (CheckIfItemExists(index))
                items[index].Use(target);
        }
        public int GetLength()
        {
            return items.Count;
        }
        private bool CheckIfItemExists(int index)
        {
            try
            {
                if(items[index] != null)
                {
                    return true;
                }

            }
            catch
            {
                return false;
            }
            return false;
        }

    }
}
